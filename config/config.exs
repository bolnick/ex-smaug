# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :smaug,
  ecto_repos: [Smaug.Repo]

# Configures the endpoint
config :smaug, SmaugWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "J8QKQC/Xmv4/poOUid/52xGk41re6MQfRFnub7y3KyIvi8IAfbjc2g0vhBbOry6N",
  render_errors: [view: SmaugWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Smaug.PubSub,
  live_view: [signing_salt: "w/+N/acx"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :shopify,
  client_id: System.get_env("SHOPIFY_PARTNER_KEY"),
  client_secret: System.get_env("SHOPIFY_PARTNER_SECRET")

# Environment Variables
config :smaug,
  big_commerce_client: SmaugWeb.Clients.BigCommerce,
  shopify_client: SmaugWeb.Clients.Shopify,
  shopify_partner_key: System.get_env("SHOPIFY_PARTNER_KEY"),
  shopify_partner_secret: System.get_env("SHOPIFY_PARTNER_SECRET"),
  temp_brand_token: System.get_env("TEMP_BRAND_ACCESS_TOKEN"),
  rabbit_url: System.get_env("RABBIT_URL")

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
