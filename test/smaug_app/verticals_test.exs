defmodule Smaug.VerticalsTest do
  use Smaug.DataCase

  alias Smaug.Verticals

  describe "verticals" do
    alias Smaug.Verticals.Vertical

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def vertical_fixture(attrs \\ %{}) do
      {:ok, vertical} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Verticals.create_vertical()

      vertical
    end

    test "list_verticals/0 returns all verticals" do
      vertical = vertical_fixture()
      assert Verticals.list_verticals() == [vertical]
    end

    test "get_vertical!/1 returns the vertical with given id" do
      vertical = vertical_fixture()
      assert Verticals.get_vertical!(vertical.id) == vertical
    end

    test "create_vertical/1 with valid data creates a vertical" do
      assert {:ok, %Vertical{} = vertical} = Verticals.create_vertical(@valid_attrs)
      assert vertical.name == "some name"
    end

    test "create_vertical/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Verticals.create_vertical(@invalid_attrs)
    end

    test "update_vertical/2 with valid data updates the vertical" do
      vertical = vertical_fixture()
      assert {:ok, %Vertical{} = vertical} = Verticals.update_vertical(vertical, @update_attrs)
      assert vertical.name == "some updated name"
    end

    test "update_vertical/2 with invalid data returns error changeset" do
      vertical = vertical_fixture()
      assert {:error, %Ecto.Changeset{}} = Verticals.update_vertical(vertical, @invalid_attrs)
      assert vertical == Verticals.get_vertical!(vertical.id)
    end

    test "delete_vertical/1 deletes the vertical" do
      vertical = vertical_fixture()
      assert {:ok, %Vertical{}} = Verticals.delete_vertical(vertical)
      assert_raise Ecto.NoResultsError, fn -> Verticals.get_vertical!(vertical.id) end
    end

    test "change_vertical/1 returns a vertical changeset" do
      vertical = vertical_fixture()
      assert %Ecto.Changeset{} = Verticals.change_vertical(vertical)
    end
  end
end
