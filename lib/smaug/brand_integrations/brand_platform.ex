import EctoEnum

defenum(Smaug.BrandIntegrations.BrandPlatform, :platform, [
  :shopify,
  :big_commerce,
  :woo_commerce,
  :magento
])
