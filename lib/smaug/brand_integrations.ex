defmodule Smaug.BrandIntegrations do
  @moduledoc """
  Context entry for brand integrations
  """

  alias Smaug.Repo
  alias Smaug.BrandIntegrations.BrandIntegration

  @doc """
  Returns the list of brand integrations.

  ## Examples

      iex> list_brand_integrations()
      [%BrandIntegration{}, ...]

  """
  def list_brand_integrations do
    Repo.all(BrandIntegration)
  end

  @doc """
  Gets a single brand integration.

  Raises `Ecto.NoResultsError` if the BrandIntegration does not exist.

  ## Examples

      iex> get_brand_integration!(123)
      %BrandIntegration{}

      iex> get_brand_integration!(456)
      ** (Ecto.NoResultsError)

  """
  def get_brand_integration(id), do: Repo.get(BrandIntegration, id)

  @doc """
  Gets a single brand integration by the brand id

  Raises `Ecto.NoResultsError` if the BrandIntegration does not exist.

  ## Examples

      iex> get_brand_integration_for_brand(123)
      %BrandIntegration{}

      iex> get_brand_integration_for_brand(456)
      ** (Ecto.NoResultsError)

  """

  def get_brand_integration_for_brand(brand_id),
    do: Repo.get_by(BrandIntegration, %{brand_id: brand_id}) |> Repo.preload(:brand)

  @doc """
  Creates a brand.

  ## Examples

      iex> create_brand_integration(%{field: value})
      {:ok, %BrandIntegration{}}

      iex> create_brand_integration(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_brand_integration(attrs \\ %{}) do
    %BrandIntegration{}
    |> BrandIntegration.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a brand_integration.

  ## Examples

      iex> update_brand_integration(brand_integration, %{field: new_value})
      {:ok, %BrandIntegration{}}

      iex> update_brand_integration(brand, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_brand_integration(%BrandIntegration{} = brand_integration, attrs) do
    brand_integration
    |> BrandIntegration.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a brand.

  ## Examples

      iex> delete_brand_integration(brand)
      {:ok, %BrandIntegration{}}

      iex> delete_brand_integration(brand_integration)
      {:error, %Ecto.Changeset{}}

  """
  def delete_brand_integration(%BrandIntegration{} = brand_integration) do
    Repo.delete(brand_integration)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking brand changes.

  ## Examples

      iex> change_brand_integration(brand_integration)
      %Ecto.Changeset{data: %BrandIntegration{}}

  """
  def change_brand_integration(%BrandIntegration{} = brand_integration, attrs \\ %{}) do
    BrandIntegration.changeset(brand_integration, attrs)
  end
end
