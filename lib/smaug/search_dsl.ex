defmodule Smaug.SearchDsl do
  @moduledoc """
  Context boundary for the product search DSL layer 
  """

  alias Smaug.BrandIntegrations.BrandIntegration
  alias Smaug.SearchDsl.{BigCommerce, Shopify}

  @client_mappings %{
    big_commerce: BigCommerce,
    shopify: Shopify
  }

  @doc """
  Entry point function for proxying the client 
  request into an external call to the connected
  e-commerce platform and inventory system.
  """
  @spec search_products(BrandIntegration.t(), map()) :: {:ok, map()} | {:error, any()}
  def search_products(%{platform: brand_platform} = brand, search_params) do
    case Map.get(@client_mappings, brand_platform) do
      dsl_client when not is_nil(dsl_client) ->
        dsl_client.call(search_params, brand)

      _err ->
        {:error, "Invalid brand platform. No connectors configured for #{brand_platform}"}
    end
  end
end
