defmodule Smaug.Repo do
  use Ecto.Repo,
    otp_app: :smaug,
    adapter: Ecto.Adapters.Postgres
end
