defmodule Smaug.SearchDsl.Shopify do
  @moduledoc """
  Client wrapper and DSL layer for handling requests
  to the Shopify API.
  """
  alias Smaug.SearchDsl.DslClient
  alias SmaugWeb.Clients.ShopifyClient

  @behaviour DslClient

  @field_mappings %{
    # NOTE: API currently supports exact match only
    name: :title
  }

  @impl DslClient
  def call(search_params, %{brand: %{name: shop_url}}) do
    with {:ok, session} <- ShopifyClient.create_session(shop_url) do
      query_params = build_query_params(search_params)

      session
      |> Shopify.Product.all(query_params)
      |> handle_response(shop_url)
    else
      err -> err
    end
  end

  def call(_, _), do: {:error, "No API connection for this brand"}

  @impl DslClient
  def handle_response({:ok, %Shopify.Response{code: 200, data: data}}, shop_url) do
    transformed_data = transform_response(shop_url, data)

    {:ok,
     %{
       data: transformed_data,
       # TODO
       size: 0
     }}
  end

  def handle_response(_, _), do: {:error, "Something broke TODO"}

  @impl DslClient
  def transform_response(shop_url, data) do
    Enum.map(data, fn product ->
      first_variant = hd(product.variants)

      # TODO: handle invalidations here
      price =
        case Float.parse(first_variant.price) do
          {val, _} -> val
          :error -> 0
        end

      image = Map.get(product, :image)
      image_url = if image, do: Map.get(image, :src), else: ""

      %{
        id: product.id,
        name: product.title,
        type: product.product_type,
        description: product.body_html,
        inventory_level: first_variant.inventory_quantity,
        # in dollars
        price: price,
        # TODO replace with shop url from table
        product_url: "https://#{shop_url}/#{product.handle}",
        image_url: image_url
      }
    end)
  end

  @spec build_query_params(map()) :: String.t()
  defp build_query_params(search_params) do
    search_params
    |> Enum.reduce(%{}, fn {key, value}, acc ->
      transformed_key = Map.get(@field_mappings, key)

      case {value, transformed_key} do
        {nil, _} -> acc
        {"", _} -> acc
        {_, nil} -> acc
        _ -> Map.put(acc, transformed_key, value)
      end
    end)
  end
end
