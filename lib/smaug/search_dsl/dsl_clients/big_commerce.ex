defmodule Smaug.SearchDsl.BigCommerce do
  @moduledoc """
  Client wrapper and DSL layer for handling requests
  to the Big Commerce API.
  """
  alias Smaug.SearchDsl.DslClient

  @behaviour DslClient

  @field_mappings %{
    name: :name
  }

  @impl DslClient
  def call(search_params, %{shop_meta: store_hash, shop_token: token}) do
    query_params = build_query_params(search_params)

    endpoint = root_endpoint(store_hash) <> "?" <> query_params <> "&include=primary_image"

    big_commerce_client().get(endpoint, auth_headers(token))
  end

  def call(_, _), do: {:error, "No API connection for this brand"}

  @impl DslClient
  def handle_response({:ok, %{body: %{data: data, meta: meta}}}, nil) do
    total_hits = get_in(meta, [:pagination, :count])
    transformed_data = transform_response("", data)

    {:ok,
     %{
       data: transformed_data,
       size: total_hits
     }}
  end

  def handle_response(_), do: {:error, "Something broke TODO"}

  @impl DslClient
  def transform_response(_, data) do
    Enum.map(data, fn product ->
      %{
        id: product.id,
        name: product.name,
        type: product.type,
        description: product.description,
        # in dollars
        inventory_level: product.inventory_level,
        price: product.price,
        # TODO replace with shop url from table
        product_url: "https://super-sweet-snacks.mybigcommerce.com/#{product.custom_url.url}",
        image_url: get_in(product, [:primary_image, :url_standard])
      }
    end)
  end

  @spec build_query_params(map()) :: String.t()
  defp build_query_params(search_params) do
    search_params
    |> Enum.reduce(%{}, fn {key, value}, acc ->
      transformed_key = Map.get(@field_mappings, key)

      case {value, transformed_key} do
        {nil, _} -> acc
        {"", _} -> acc
        {_, nil} -> acc
        _ -> Map.put(acc, transformed_key, value)
      end
    end)
    |> URI.encode_query()
  end

  defp big_commerce_client, do: Application.get_env(:smaug, :big_commerce_client)
  defp root_endpoint(store_hash), do: "#{store_hash}/v3/catalog/products"
  defp auth_headers(token), do: [{"X-Auth-Token", token}]
end
