defmodule SmaugWeb.Resolvers.Brands do
  @moduledoc """
  Resolver for brand related queries
  """
  alias Smaug.Brands

  @type brands_input :: %{
          name: String.t(),
          platform: String.t(),
          shop_url: String.t(),
          shop_token: String.t(),
          shop_meta: String.t()
        }

  @doc """
  Resolver for retrieving a brand
  """
  @spec get_brand(map(), map()) :: {:ok, map()} | {:error, nil}
  def get_brand(%{id: brand_id}, _context) do
    brand = Brands.get_brand(brand_id)
    {:ok, brand}
  end

  @doc """
  Resolver for getting all brands
  """
  @spec all_brands(map(), map()) :: {:ok, map()} | {:error, nil}
  def all_brands(_args, _context) do
    brands = Brands.list_brands()
    {:ok, brands}
  end

  @doc """
  Resolver for creating a brand
  """
  @spec create_brand(brands_input(), map()) :: {:ok, map()} | {:error, nil}
  def create_brand(args, _context), do: Brands.create_brand(args)
end
