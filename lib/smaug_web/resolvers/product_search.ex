defmodule SmaugWeb.Resolvers.ProductSearch do
  @moduledoc """
  Resolver for product search related queries
  """

  alias Smaug.BrandIntegrations
  alias Smaug.BrandIntegrations.BrandIntegration
  alias Smaug.SearchDsl

  @doc """
  Main entry point for product search handlers
  """
  @spec search_products(map(), map()) :: {:ok, map()} | {:error, nil}
  def search_products(%{brand_id: brand_id} = args, _context) do
    case BrandIntegrations.get_brand_integration_for_brand(brand_id) do
      %BrandIntegration{} = brand_integration ->
        SearchDsl.search_products(brand_integration, args)

      _err ->
        {:error, "Brand not found"}
    end
  end
end
