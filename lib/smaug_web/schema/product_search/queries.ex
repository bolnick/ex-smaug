defmodule SmaugWeb.Schema.ProductSearch.Queries do
  @moduledoc """
  Query schema for external product searches
  """
  use Absinthe.Schema.Notation
  alias SmaugWeb.Resolvers.ProductSearch

  object :product_search_queries do
    field :search_products_query, :product_search_response do
      arg(:brand_id, non_null(:id))
      arg(:name, :string)
      # arg(:product_id, :string)

      # TODO check on is_visible for all searches when querying
      resolve(&ProductSearch.search_products/2)
    end
  end
end
