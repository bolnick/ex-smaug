defmodule SmaugWeb.Schema.ProductSearch.Types do
  @moduledoc """
  Types for product search schema
  """
  use Absinthe.Schema.Notation

  object :product_search_response do
    field(:data, list_of(:product_result))
    field(:size, :integer)
  end

  object :product_result do
    field :id, :id
    field :name, :string
    field :type, :string
    field :description, :string
    field :inventory_level, :integer
    field :price, :float
    field :product_url, :string
    field :image_url, :string
    # field :variants, list_of(:variant)
  end
end
