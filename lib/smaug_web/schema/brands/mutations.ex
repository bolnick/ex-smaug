defmodule SmaugWeb.Schema.Brands.Mutations do
  @moduledoc """
  Brands private mutation schema
  """
  use Absinthe.Schema.Notation
  alias SmaugWeb.Resolvers.Brands

  object :brand_mutations do
    @desc "Create a brand"
    field :create_brand, :brand do
      arg(:name, non_null(:string))
      arg(:platform, non_null(:string))
      arg(:shop_url, :string)
      arg(:shop_token, :string)
      arg(:shop_meta, :string)

      resolve(&Brands.create_brand/2)
    end
  end
end
