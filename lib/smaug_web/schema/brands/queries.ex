defmodule SmaugWeb.Schema.Brands.Queries do
  @moduledoc """
  Brand query schema
  """
  use Absinthe.Schema.Notation
  alias SmaugWeb.Resolvers.Brands

  object :brand_queries do
    field :brand_query, :brand do
      arg(:id, non_null(:id))

      resolve(&Brands.get_brand/2)
    end

    field :all_brands_query, list_of(:brand) do
      resolve(&Brands.all_brands/2)
    end
  end
end
