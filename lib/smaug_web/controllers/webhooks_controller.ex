defmodule SmaugWeb.WebhooksController do
  use SmaugWeb, :controller
  require Logger

  alias Smaug.Webhooks

  action_fallback(SmaugWeb.FallbackController)

  def stripe(conn, %{"type" => type} = payload) do
    Logger.info("[Stripe Webhooks]: Recieved payload from stripe: #{type}")
    Webhooks.handle_stripe_payload(type, payload)
    json(conn, %{success: true})
  end
end
