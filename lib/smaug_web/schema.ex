defmodule SmaugWeb.Schema do
  @moduledoc """
  GraphQL Schema

  Use the build in absinthe import_types and import_fields
  https://hexdocs.pm/absinthe/Absinthe.Schema.Notation.html
  """

  use Absinthe.Schema

  import_types(SmaugWeb.Schema.Brands.Types)
  import_types(SmaugWeb.Schema.Brands.Queries)
  import_types(SmaugWeb.Schema.Brands.Mutations)

  import_types(SmaugWeb.Schema.ProductSearch.Types)
  import_types(SmaugWeb.Schema.ProductSearch.Queries)

  query do
    import_fields(:brand_queries)
    import_fields(:product_search_queries)
  end

  mutation do
    import_fields(:brand_mutations)
  end
end
