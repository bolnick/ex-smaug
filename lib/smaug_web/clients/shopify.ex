defmodule SmaugWeb.Clients.ShopifyClient do
  @moduledoc """
  Client wrapper for making HTTP requests to Shopify
  """

  @spec create_session(String.t()) :: {:ok, %Shopify.Session{}} | {:error, any()}
  def create_session(shop) do
    case Shopify.session(shop, brand_token()) do
      %Shopify.Session{access_token: nil} ->
        {:error, "Session not created, token invalid"}

      %Shopify.Session{access_token: _token} = session ->
        {:ok, session}

      err ->
        {:error, "There was an issue creating the shopify session: #{inspect(err)}"}
    end
  end

  # TODO: fetch from sensus-dev after SSM config setup 
  defp brand_token, do: Application.get_env(:smaug, :temp_brand_token)
end
