defmodule SmaugWeb.Clients.BigCommerce do
  @moduledoc """
  Client wrapper for making HTTP requests to BigCommerce
  """
  use HTTPoison.Base

  def process_request_url(path), do: root_url() <> path

  def process_request_body(body) when is_map(body), do: Poison.encode!(body)
  def process_request_body(body), do: body

  def process_request_headers(headers),
    do: [
      {"Accept", "application/json"},
      {"Content-Type", "application/json"} | headers
    ]

  def process_response_body(""), do: ""

  def process_response_body(body) do
    body
    |> Poison.decode(keys: :atoms)
    |> case do
      {:ok, response} ->
        response

      error ->
        error
    end
  end

  def process_response_headers(headers), do: Map.new(headers)

  defp root_url, do: "https://api.bigcommerce.com/stores/"
end
